'use strict';

const fs = require('fs');
const path = require('path');
const util = require('./util');

const baseDir = util.getBaseDir();

(function updateVersion() {
	const packageJson = util.readPackageJson();
	const moduleJsonPath = path.join(baseDir, 'src', 'module.json');
	const moduleJson = JSON.parse(fs.readFileSync(moduleJsonPath));

	moduleJson.version = packageJson.version;

	fs.writeFileSync(moduleJsonPath, JSON.stringify(moduleJson, null, '\t') + '\n');
})();

