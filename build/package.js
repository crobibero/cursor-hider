'use strict';

const fs = require('fs');
const path = require('path');
const archiver = require('archiver');
const util = require('./util');

const baseDir = path.join(__dirname, '..');


(function createModuleZip() {
	const packageJson = util.readPackageJson();
	fs.mkdir(path.join(baseDir, 'dist'), '0777', function(err) {
		if (err && err.code !== 'EEXIST') throw new Error(err);
	});
	let zipPath = path.join(baseDir, 'dist', packageJson.name + '.zip');
	const zipFile = fs.createWriteStream(zipPath);
	const archive = archiver('zip');
	archive.pipe(zipFile);
	archive.directory('src/', packageJson.name);
	archive.finalize();
})();

