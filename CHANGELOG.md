# 1.1.4

* Update to latest version of settings-extender

# 1.1.3

* Compatibility with FoundryVTT 0.4.4

# 1.1.2

* Compatibility with FoundryVTT 0.4.3

# 1.1.1

* Update dependency

# 1.1.0

* Add hover text to "cursor hidden" image in the player list 
