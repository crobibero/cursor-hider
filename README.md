A [FoundryVTT](http://foundryvtt.com/) module to give the users the ability to hide their cursors on the tabletop individually, instead of only being able to hide all or none.

Useful for example if you still want cursors in general, but you're sometimes messing with off-screen enemies as a Gamemaster and don't want your players to see.

See the [Features section](#features) for more info.

# Installation

## Recommended

1. Go to Foundry's Setup screen
1. Go to the "Add-On Modules" tab
1. Press "Install Module"
1. Paste `https://gitlab.com/foundry-azzurite/cursor-hider/raw/master/src/module.json` into the text field
1. Press "Install"

## Alternative

1. Download [this zip file](https://gitlab.com/foundry-azzurite/cursor-hider/raw/master/dist/pings.zip)
2. Extract it into the `<FoundryVTT directory>/resources/app/public/modules`-folder

# Features

Hide your cursor and your ruler with the press of a button (default Alt + C). 

![demonstration](doc/cursor-hider.mp4)

You can configure this key, allow the hiding of the cursor only for certain permissions and hide your cursor by default:

![settings](doc/settings.png) 
